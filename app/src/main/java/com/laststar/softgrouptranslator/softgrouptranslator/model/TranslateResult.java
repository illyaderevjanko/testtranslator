package com.laststar.softgrouptranslator.softgrouptranslator.model;

import java.util.List;

/**
 * Created by laststar on 05.01.17.
 */

public class TranslateResult {
    private int code;
    private String lang;
    private List<String> text;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public List<String> getText() {
        return text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }
}
