package com.laststar.softgrouptranslator.softgrouptranslator.di.modules;

import android.content.Context;

import com.laststar.softgrouptranslator.softgrouptranslator.tools.NetworkConnectionTools;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by last star on 22.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

@Module
public class NetworkConnectionModule {

    @Provides
    @Singleton
    public NetworkConnectionTools getNetworkConnection(Context context) {
        return new NetworkConnectionTools(context);
    }
}
