package com.laststar.softgrouptranslator.softgrouptranslator.di.modules;

import android.content.Context;
import android.content.res.Resources;

import org.xmlpull.v1.XmlPullParser;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by last star on 22.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

@Module
public class ResourcesModule {

    @Provides
    @Singleton
    public Resources getResources(Context context) {
        return context.getResources();
    }
}
