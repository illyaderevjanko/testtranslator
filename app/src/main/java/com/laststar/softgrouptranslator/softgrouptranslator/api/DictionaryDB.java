package com.laststar.softgrouptranslator.softgrouptranslator.api;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.laststar.softgrouptranslator.softgrouptranslator.consts.DictionaryDBConts;

/**
 * Created by laststar on 07.01.17.
 */

public class DictionaryDB extends SQLiteOpenHelper {

    public DictionaryDB(Context context) {
        super(context, DictionaryDBConts.DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + DictionaryDBConts.TABLE_DICTIONARY + " ("
                + DictionaryDBConts.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + DictionaryDBConts.FIELD_LANG_CODE + " CHAR(10) NOT NULL,"
                + DictionaryDBConts.FIELD_TEXT_FROM + " TEXT NOT NULL,"
                + DictionaryDBConts.FIELD_TEXT_TO + " TEXT NOT NULL"
                + ");");

        db.execSQL("create table " + DictionaryDBConts.TABLE_LANGUAGES + " ("
                + DictionaryDBConts.FIELD_CODE + " CHAR(5),"
                + DictionaryDBConts.FIELD_LANG + " CHAR(50) NOT NULL"
                + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
