package com.laststar.softgrouptranslator.softgrouptranslator.tools.shared_pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.laststar.softgrouptranslator.softgrouptranslator.consts.SharedPrefConst;

/**
 * Created by laststar on 06.01.17.
 */

public class Load {
    private SharedPreferences sharedPreferences;
    private Context context;

    public Load(Context context) {
        this.context = context;
    }

    public boolean loadIsDictionariesInDB() {
        sharedPreferences = context.getSharedPreferences(SharedPrefConst.SAVE_TO_DB_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(SharedPrefConst.SAVE_TO_BD_KEY, false);
    }
}
