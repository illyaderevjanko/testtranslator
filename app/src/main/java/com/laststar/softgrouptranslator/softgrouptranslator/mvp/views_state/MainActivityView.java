package com.laststar.softgrouptranslator.softgrouptranslator.mvp.views_state;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.laststar.softgrouptranslator.softgrouptranslator.model.Languages;
import com.laststar.softgrouptranslator.softgrouptranslator.model.TranslateResult;

/**
 * Created by laststar on 05.01.17.
 */

@StateStrategyType(AddToEndSingleStrategy.class)
public interface MainActivityView extends MvpView {
    void translateResult(TranslateResult translateResult, boolean isDictionary);
    void setLanguages(Languages languages);
    void noInternetConnectionAndLangFromEmpty();
}
