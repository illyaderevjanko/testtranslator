package com.laststar.softgrouptranslator.softgrouptranslator.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.laststar.softgrouptranslator.softgrouptranslator.R;
import com.laststar.softgrouptranslator.softgrouptranslator.interfaces.SimpleChoice;
import com.laststar.softgrouptranslator.softgrouptranslator.model.Languages;
import com.laststar.softgrouptranslator.softgrouptranslator.model.TranslateResult;
import com.laststar.softgrouptranslator.softgrouptranslator.mvp.MainActivityPresenter;
import com.laststar.softgrouptranslator.softgrouptranslator.view.dialogs.Dialogs;
import com.laststar.softgrouptranslator.softgrouptranslator.mvp.views_state.MainActivityView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends MvpAppCompatActivity implements MainActivityView {
    private String LANG_FROM = "Определить язык";
    private String LANG_TO = "Русский";
    private String LANG_FROM_CODE = "";
    private String LANG_TO_CODE = "ru";

    @BindView(R.id.lang_from)
    TextView langFrom;
    @BindView(R.id.lang_to)
    TextView langTo;
    @BindView(R.id.et_query)
    EditText etQuery;
    @BindView(R.id.txt_result)
    TextView txtResult;
    @BindView(R.id.btn_clear_query)
    ImageView btnClearQuery;
    @BindView(R.id.btn_add_to_dictionary)
    ImageView btnAddToDictionary;

    @BindString(R.string.select_language)
    String selectLanguage;
    @BindString(R.string.target_language)
    String targetLanguage;
    @BindString(R.string.not_internet_select_lang_from)
    String notInternetSelectLangFrom;

    private boolean isTranslated = false;

    private TranslateResult translateResult;
    private Languages languages;
    private Dialogs dialogs;
    @InjectPresenter
    MainActivityPresenter presenter;
    private Unbinder unbinder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(this);

        initViews();
        setLang(false);
    }

    private void initViews(){
        dialogs = new Dialogs(this);
        btnClearQuery.setVisibility(View.GONE);
        btnAddToDictionary.setVisibility(View.GONE);
        etQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(isTranslated){
                    isTranslated = false;
                    btnAddToDictionary.setVisibility(View.GONE);
                }
                if(editable.toString().isEmpty()){
                    btnClearQuery.setVisibility(View.GONE);
                } else {
                    btnClearQuery.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void setLang(boolean isSwap){
        if(isSwap) {
            if(!LANG_FROM_CODE.isEmpty()) {
                //        change lang
                String temp = LANG_FROM;
                LANG_FROM = LANG_TO;
                LANG_TO = temp;
                langFrom.setText(LANG_FROM);
                langTo.setText(LANG_TO);
                //        change language code
                temp = LANG_FROM_CODE;
                LANG_FROM_CODE = LANG_TO_CODE;
                LANG_TO_CODE = temp;
                //        swap text in views
                if (!txtResult.getText().toString().isEmpty()) {
                    etQuery.setText(txtResult.getText().toString());
                    txtResult.setText("");
                }
            }
        } else {
            langFrom.setText(LANG_FROM);
            langTo.setText(LANG_TO);
        }
    }

    private void chooseLanguage(boolean isFrom){
        if(isFrom){
            dialogs.chooseLang(languages, new SimpleChoice() {
                @Override
                public void languageIsChosen(String name, String lang_code) {
                    LANG_FROM = name;
                    LANG_FROM_CODE = lang_code;
                    langFrom.setText(LANG_FROM);
                }
            }, selectLanguage);
        } else {
            dialogs.chooseLang(languages, new SimpleChoice() {
                @Override
                public void languageIsChosen(String name, String lang_code) {
                    LANG_TO = name;
                    LANG_TO_CODE = lang_code;
                    langTo.setText(LANG_TO);
                }
            }, targetLanguage);
        }
    }

    private void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etQuery.getWindowToken(), 0);
    }

    private List<String> getListQueries(){
        List<String> text = new ArrayList<>();
        String query = etQuery.getText().toString();
        if(!query.trim().isEmpty()) {
            String[] query_lines = query.split("\\n");
            for (String query_line : query_lines) {
                if (!query_line.isEmpty()) {
                    text.add(query_line);
                }
            }

            return text;
        }
        return null;
    }

    private String getLang(){
        String lang = "";
        if (!LANG_FROM_CODE.isEmpty()) {
            lang = LANG_FROM_CODE + "-" + LANG_TO_CODE;
        } else {
            lang = LANG_TO_CODE;
        }
        return lang;
    }

    @Override
    public void translateResult(TranslateResult translateResult, boolean isDictionary) {
        this.translateResult = translateResult;
        isTranslated = true;
        if(LANG_FROM_CODE.isEmpty()){
            String[] lang_array = translateResult.getLang().split("-");
            LANG_FROM_CODE = lang_array[0];
            LANG_FROM = languages.getLangs().get(LANG_FROM_CODE);
            langFrom.setText(LANG_FROM);
        }
        StringBuilder result = new StringBuilder();
        for(String item : translateResult.getText()){
            result.append(item);
            result.append("\n");
        }
        txtResult.setText(result);
        if(!isDictionary) {
            btnAddToDictionary.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setLanguages(Languages languages) {
        this.languages = languages;
    }

    @Override
    public void noInternetConnectionAndLangFromEmpty() {
        txtResult.setText(notInternetSelectLangFrom);
    }

    @OnClick(R.id.lang_from)
    protected void setLangFrom(){
        if(languages != null) {
            chooseLanguage(true);
        }
    }

    @OnClick(R.id.lang_to)
    protected void setLangTo(){
        if(languages != null) {
            chooseLanguage(false);
        }
    }

    @OnClick(R.id.swap_lang)
    protected void swapLang(){
        setLang(true);
    }

    @OnClick(R.id.btn_clear_query)
    protected void clearQuery(){
        etQuery.setText("");
    }

    @OnClick(R.id.btn_add_to_dictionary)
    protected void addTextToDictionary(){
        presenter.addTextToDictionary(getListQueries(), translateResult.getText(), getLang());
        btnAddToDictionary.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_translate)
    protected void translate(){
        if(getListQueries() != null) {
            presenter.translate(getListQueries(), getLang(), LANG_FROM_CODE);
        }
        hideSoftKeyboard();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.closeDB();
        unbinder.unbind();
    }
}
