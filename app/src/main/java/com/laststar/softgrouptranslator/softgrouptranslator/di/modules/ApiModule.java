package com.laststar.softgrouptranslator.softgrouptranslator.di.modules;

import android.content.Context;

import com.laststar.softgrouptranslator.softgrouptranslator.api.DictionaryDB;
import com.laststar.softgrouptranslator.softgrouptranslator.api.YandexTranslatorApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by last star on 22.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

@Module
public class ApiModule {
    @Provides
    @Singleton
    public YandexTranslatorApi getApi() {
        return YandexTranslatorApi.TranslatorApi.create();
    }

    @Provides
    @Singleton
    public DictionaryDB getLocalDB(Context context) {
        return new DictionaryDB(context);
    }
}
