package com.laststar.softgrouptranslator.softgrouptranslator.tools;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by last star on 22.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

public class NetworkConnectionTools {
    private Context context;

    public NetworkConnectionTools(Context context) {
        this.context = context;
    }

    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }
}
