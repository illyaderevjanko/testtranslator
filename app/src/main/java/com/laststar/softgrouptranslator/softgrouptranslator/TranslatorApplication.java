package com.laststar.softgrouptranslator.softgrouptranslator;

import android.app.Application;

import com.laststar.softgrouptranslator.softgrouptranslator.di.components.AppComponent;
import com.laststar.softgrouptranslator.softgrouptranslator.di.components.DaggerAppComponent;
import com.laststar.softgrouptranslator.softgrouptranslator.di.modules.ContextModule;

/**
 * Created by laststar on 05.01.17.
 */

public class TranslatorApplication extends Application {
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = buildComponent();
    }

    private AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
