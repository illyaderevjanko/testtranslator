package com.laststar.softgrouptranslator.softgrouptranslator.consts;

/**
 * Created by laststar on 05.01.17.
 */

public class YandexApiConst {
    public static final String HOST_NAME = "https://translate.yandex.net/";
    public static final String API = "api/";
    public static final String VERSION = "v1.5/";
    public static final String JSON = "tr.json/";

    public static final String TRANSLATE = "translate";
    public static final String GET_LANGS = "getLangs";
    public static final String KEY = "key";
    public static final String UI = "ui";
    public static final String TEXT = "text";
    public static final String LANG = "lang";
    public static final String FORMAT = "format";
    public static final String FORMAT_PLAIN = "plain";
}
