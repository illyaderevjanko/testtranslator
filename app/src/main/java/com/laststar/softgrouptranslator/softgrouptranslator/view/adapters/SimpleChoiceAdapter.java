package com.laststar.softgrouptranslator.softgrouptranslator.view.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.laststar.softgrouptranslator.softgrouptranslator.R;
import com.laststar.softgrouptranslator.softgrouptranslator.interfaces.SimpleChoice;
import com.laststar.softgrouptranslator.softgrouptranslator.model.Languages;
import com.laststar.softgrouptranslator.softgrouptranslator.view.dialogs.Dialogs;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by laststar on 03.01.17.
 */

public class SimpleChoiceAdapter extends RecyclerView.Adapter<SimpleChoiceAdapter.ItemHolder> {
    private Context context;
    private Dialog dialog;
    private Languages languages;
    private String[] keys;
    private SimpleChoice simpleChoice;
    private LayoutInflater layoutInflater;

    public SimpleChoiceAdapter(Context context, Dialog dialog, Languages languages, SimpleChoice simpleChoice) {
        this.context = context;
        this.dialog = dialog;
        this.languages = languages;
        this.keys = languages.getLangs().keySet().toArray(new String[languages.getLangs().size()]);
        this.simpleChoice = simpleChoice;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemHolder(layoutInflater.inflate(R.layout.item_list_simple, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {
        final String key = keys[position];
        final String value = languages.getLangs().get(key);
        holder.txtLanguage.setText(value);
        holder.txtLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                simpleChoice.languageIsChosen(value, key);
                Dialogs.dialogIsShow = false;
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return languages.getLangs().size();
    }


    class ItemHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_language)
        TextView txtLanguage;

        ItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
