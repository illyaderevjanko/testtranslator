package com.laststar.softgrouptranslator.softgrouptranslator.view.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.laststar.softgrouptranslator.softgrouptranslator.R;
import com.laststar.softgrouptranslator.softgrouptranslator.interfaces.SimpleChoice;
import com.laststar.softgrouptranslator.softgrouptranslator.model.Languages;
import com.laststar.softgrouptranslator.softgrouptranslator.view.adapters.SimpleChoiceAdapter;

/**
 * Created by laststar on 05.01.17.
 */

public class Dialogs {
    private Context context;
    public static boolean dialogIsShow = false;

    public Dialogs(Context context) {
        this.context = context;
    }

    public void chooseLang(Languages languages, SimpleChoice simpleChoice, String toolbarTitle){
        View view = View.inflate(context, R.layout.dialog_select_language, null);
        final Dialog dialog = new Dialog(context, R.style.DialogFullScreen);
        dialog.setContentView(view);

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(toolbarTitle);
        RecyclerView listLanguages = (RecyclerView) view.findViewById(R.id.list_languages);
        listLanguages.setLayoutManager(new LinearLayoutManager(context));
        SimpleChoiceAdapter simpleChoiceAdapter = new SimpleChoiceAdapter(context, dialog, languages, simpleChoice);
        listLanguages.setAdapter(simpleChoiceAdapter);

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                dialogIsShow = false;
            }
        });

        if(!dialogIsShow) {
            dialog.show();
        }
        dialogIsShow = true;
    }
}