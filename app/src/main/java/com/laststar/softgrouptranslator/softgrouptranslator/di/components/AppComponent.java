package com.laststar.softgrouptranslator.softgrouptranslator.di.components;

import com.laststar.softgrouptranslator.softgrouptranslator.di.modules.ApiModule;
import com.laststar.softgrouptranslator.softgrouptranslator.di.modules.ContextModule;
import com.laststar.softgrouptranslator.softgrouptranslator.di.modules.NetworkConnectionModule;
import com.laststar.softgrouptranslator.softgrouptranslator.di.modules.ResourcesModule;
import com.laststar.softgrouptranslator.softgrouptranslator.di.modules.SharedPrefModule;
import com.laststar.softgrouptranslator.softgrouptranslator.di.modules.YandexApiInfoModule;
import com.laststar.softgrouptranslator.softgrouptranslator.mvp.MainActivityPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by last star on 22.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

@Component(modules = {ContextModule.class, ApiModule.class, SharedPrefModule.class, YandexApiInfoModule.class, NetworkConnectionModule.class, ResourcesModule.class})
@Singleton
public interface AppComponent {
    void inject(MainActivityPresenter mainActivityPresenter);
}
