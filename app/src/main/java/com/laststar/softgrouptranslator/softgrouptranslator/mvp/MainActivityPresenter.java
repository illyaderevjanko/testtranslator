package com.laststar.softgrouptranslator.softgrouptranslator.mvp;

import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteStatement;

import com.arellomobile.mvp.InjectViewState;
import com.laststar.softgrouptranslator.softgrouptranslator.R;
import com.laststar.softgrouptranslator.softgrouptranslator.TranslatorApplication;
import com.laststar.softgrouptranslator.softgrouptranslator.api.YandexTranslatorApi;
import com.laststar.softgrouptranslator.softgrouptranslator.consts.DictionaryDBConts;
import com.laststar.softgrouptranslator.softgrouptranslator.api.DictionaryDB;
import com.laststar.softgrouptranslator.softgrouptranslator.consts.YandexApiConst;
import com.laststar.softgrouptranslator.softgrouptranslator.model.Languages;
import com.laststar.softgrouptranslator.softgrouptranslator.model.TranslateResult;
import com.laststar.softgrouptranslator.softgrouptranslator.tools.NetworkConnectionTools;
import com.laststar.softgrouptranslator.softgrouptranslator.tools.YandexApiTools;
import com.laststar.softgrouptranslator.softgrouptranslator.tools.shared_pref.Load;
import com.laststar.softgrouptranslator.softgrouptranslator.tools.shared_pref.Save;
import com.laststar.softgrouptranslator.softgrouptranslator.mvp.views_state.MainActivityView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by laststar on 05.01.17.
 */

@InjectViewState
public class MainActivityPresenter extends BasePresenter<MainActivityView> {
    private TranslateResult result;
    private Languages languages;

    @Inject
    Load load;
    @Inject
    Save save;
    @Inject
    YandexApiTools yandexApiTools;
    @Inject
    DictionaryDB localDB;
    @Inject
    YandexTranslatorApi api;
    @Inject
    NetworkConnectionTools networkConnectionTools;
    @Inject
    Resources resources;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        if(!load.loadIsDictionariesInDB()) {
            parserXml(save);
        }
        if(networkConnectionTools.isConnected()) {
            getLanguages();
        } else {
            getLanguagesFromDB();
        }
    }

    public MainActivityPresenter() {
        TranslatorApplication.getAppComponent().inject(this);
    }

    private void getLanguages() {
        api.getLanguages(yandexApiTools.getApiKey(), "ru")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableObserver<Response<Languages>>() {
                    @Override
                    public void onComplete() {
                        addLanguagesToDB();
                        HashMap<String, String> langMap = Languages.sortByValue(languages.getLangs());
                        langMap.put("", "Определить язык");
                        languages.setLangs(langMap);
                        getViewState().setLanguages(languages);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<Languages> languagesResponse) {
                        languages = languagesResponse.body();
                    }
                });
    }

    public void translate(List<String> text, String lang, String langFromCode) {
        if(networkConnectionTools.isConnected()) {
            api.translate(yandexApiTools.getApiKey(), text, lang, YandexApiConst.FORMAT_PLAIN)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new DisposableObserver<Response<TranslateResult>>() {
                        @Override
                        public void onComplete() {
                            getViewState().translateResult(result, false);
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(Response<TranslateResult> translateResultResponse) {
                            result = translateResultResponse.body();
                        }
                    });
        } else {
            if(!langFromCode.isEmpty()) {
                loadTextFromDictionary(text, lang);
            } else {
                getViewState().noInternetConnectionAndLangFromEmpty();
            }
        }
    }

    private void parserXml(Save save){
        SQLiteDatabase db = localDB.getWritableDatabase();
        boolean isRusWord = false;
        boolean isEngWord = false;
        boolean isLastEng = false;
        int translate_variants = 1;
        String rus = "";
        String eng = "";
        String query = "INSERT INTO " + DictionaryDBConts.TABLE_DICTIONARY
                + " ("
                + DictionaryDBConts.FIELD_LANG_CODE
                + ", " + DictionaryDBConts.FIELD_TEXT_FROM
                + ", " + DictionaryDBConts.FIELD_TEXT_TO
                + ")"
                + " VALUES (?, ?, ?)";
        db.beginTransaction();
        SQLiteStatement stmt= db.compileStatement(query);
        try {
            XmlPullParser xpp = resources.getXml(R.xml.en_rus);
            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (xpp.getEventType()) {
                    case XmlPullParser.START_TAG:
                        isEngWord = false;
                        isRusWord = false;
                        if(xpp.getName().equals("entry")){
                            stmt.bindString(1, "en-ru");
                            stmt.bindString(2, eng);
                            stmt.bindString(3, rus);
                            stmt.execute();
                            stmt.clearBindings();
                        }
                        if(xpp.getName().equals("orth")) {
                            isEngWord = true;
                            if(isLastEng){
                                translate_variants++;
                            } else {
                                translate_variants = 1;
                            }
                            isLastEng = true;
                        } else if(xpp.getName().equals("quote")){
                            isRusWord = true;
                            if(!isLastEng){
                                translate_variants++;
                            } else {
                                translate_variants = 1;
                            }
                            isLastEng = false;
                        }
                        break;
                    case XmlPullParser.TEXT:
                        if(isEngWord) {
                            if(translate_variants > 1) {
                                if(!eng.contains(xpp.getText())) {
                                    eng = eng + ", " + xpp.getText();
                                }
                            } else {
                                eng = xpp.getText();
                            }
                        } else if(isRusWord){
                            if(translate_variants > 1) {
                                if(!rus.contains(xpp.getText())) {
                                    rus = rus + ", " + xpp.getText();
                                }
                            } else {
                                rus = xpp.getText();
                            }
                        }
                        break;
                }
                // следующий элемент
                xpp.next();
            }
            stmt.bindString(1, "en-ru");
            stmt.bindString(2, eng);
            stmt.bindString(3, rus);
            stmt.execute();
            stmt.clearBindings();
            db.setTransactionSuccessful();
            db.endTransaction();
            save.saveIsDictionariesInDB();
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
    }

    private void loadTextFromDictionary(List<String> text, String lang_code){
        List<String> translateText = new ArrayList<>();
        SQLiteDatabase db = localDB.getWritableDatabase();
        int size = text.size();
        String querySelect = "SELECT " + DictionaryDBConts.FIELD_TEXT_TO
                + " FROM " + DictionaryDBConts.TABLE_DICTIONARY
                + " WHERE " + DictionaryDBConts.FIELD_LANG_CODE + " = ?"
                + " AND " + DictionaryDBConts.FIELD_TEXT_FROM + " = ?"
                + " LIMIT 1";
        SQLiteStatement stmtSelect = db.compileStatement(querySelect);
        db.beginTransaction();
        for(int i=0; i<size; i++) {
            stmtSelect.bindString(1, lang_code);
            stmtSelect.bindString(2, text.get(i));
            try {
                String result = stmtSelect.simpleQueryForString();
                translateText.add(result);
            } catch (SQLiteDoneException exception){
                translateText.add("Нет в слова/текста в словаре.");
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        stmtSelect.close();
        db.close();
        result = new TranslateResult();
        result.setCode(200);
        result.setText(translateText);
        result.setLang(lang_code);
        getViewState().translateResult(result, true);
    }

    public void addTextToDictionary(List<String> text, List<String> translateText, String lang_code){
        SQLiteDatabase db = localDB.getWritableDatabase();
        int size = text.size();
        String querySelect = "SELECT EXISTS ("
                +"SELECT *"
                + " FROM " + DictionaryDBConts.TABLE_DICTIONARY
                + " WHERE " + DictionaryDBConts.FIELD_LANG_CODE + " = ?"
                + " AND " + DictionaryDBConts.FIELD_TEXT_FROM + " = ?"
                + " AND " + DictionaryDBConts.FIELD_TEXT_TO + " = ?"
                + " LIMIT 1);";
        SQLiteStatement stmtSelect = db.compileStatement(querySelect);
        String queryInsert = "INSERT INTO " + DictionaryDBConts.TABLE_DICTIONARY
                + " ("
                + DictionaryDBConts.FIELD_LANG_CODE
                + ", " + DictionaryDBConts.FIELD_TEXT_FROM
                + ", " + DictionaryDBConts.FIELD_TEXT_TO
                + ")"
                + " VALUES (?, ?, ?)";
        SQLiteStatement stmtInsert = db.compileStatement(queryInsert);
        db.beginTransaction();
        for(int i=0; i<size; i++){
            stmtSelect.bindString(1, lang_code);
            stmtSelect.bindString(2, text.get(i));
            stmtSelect.bindString(3, translateText.get(i));
            long result = stmtSelect.simpleQueryForLong();
            if(result == 0){
                stmtInsert.bindString(1, lang_code);
                stmtInsert.bindString(2, text.get(i));
                stmtInsert.bindString(3, translateText.get(i));
                stmtInsert.execute();
                stmtInsert.clearBindings();
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        stmtSelect.close();
        stmtInsert.close();
        db.close();
    }

    private void addLanguagesToDB(){
        SQLiteDatabase db = localDB.getWritableDatabase();
        String query = "REPLACE INTO " + DictionaryDBConts.TABLE_LANGUAGES
                + " ("
                + DictionaryDBConts.FIELD_CODE
                + ", " + DictionaryDBConts.FIELD_LANG
                + ")"
                + " VALUES (?, ?)";
        db.beginTransaction();
        SQLiteStatement stmt = db.compileStatement(query);
        for(String key : languages.getLangs().keySet()){
            stmt.bindString(1, key);
            stmt.bindString(2, languages.getLangs().get(key));
            stmt.execute();
            stmt.clearBindings();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        stmt.close();
        db.close();
    }

    private void getLanguagesFromDB(){
        SQLiteDatabase db = localDB.getWritableDatabase();
        String query = "SELECT * "
                + " FROM " + DictionaryDBConts.TABLE_LANGUAGES;
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            HashMap<String, String> languagesArray = new HashMap<>();
            int columnCode = cursor.getColumnIndex(DictionaryDBConts.FIELD_CODE);
            int columnLang = cursor.getColumnIndex(DictionaryDBConts.FIELD_LANG);
            while (cursor.moveToNext()){
                languagesArray.put(cursor.getString(columnCode), cursor.getString(columnLang));
            }
            languages = new Languages();
            HashMap<String, String> langMap = Languages.sortByValue(languagesArray);
            langMap.put("", "Определить язык");
            languages.setLangs(langMap);
        }
        cursor.close();
        db.close();
        getViewState().setLanguages(languages);
    }

    public void closeDB(){
        localDB.close();
    }
}
