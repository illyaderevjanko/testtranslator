package com.laststar.softgrouptranslator.softgrouptranslator.di.modules;

import android.content.Context;

import com.laststar.softgrouptranslator.softgrouptranslator.tools.shared_pref.Load;
import com.laststar.softgrouptranslator.softgrouptranslator.tools.shared_pref.Save;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by last star on 22.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

@Module
public class SharedPrefModule {

    @Provides
    @Singleton
    public Load loadPref(Context context) {
        return new Load(context);
    }

    @Provides
    @Singleton
    public Save savePref(Context context) {
        return new Save(context);
    }
}
