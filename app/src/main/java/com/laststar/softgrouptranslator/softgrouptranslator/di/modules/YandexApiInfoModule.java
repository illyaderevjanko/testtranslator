package com.laststar.softgrouptranslator.softgrouptranslator.di.modules;

import com.laststar.softgrouptranslator.softgrouptranslator.tools.YandexApiTools;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by last star on 22.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

@Module
public class YandexApiInfoModule {
    @Provides
    @Singleton
    public YandexApiTools getYandexApiHelper() {
        return new YandexApiTools();
    }
}
